## Setup

* Create a source and destination repo if it does not exist.
* Create a key-pair
    ```sh
    ssh-keygen -t ed25519 -C "CICD Key"
    ```
* Go to destination repo and create a deploy key by going to `Settings>Repository`. Copy the public part of the created key and enable Write access.
* Go to the source repo
    * `Settings>CI/CD>Variables`
    * Add key, Key>SSH_PUSH_KEY and Value>Private part of the key-pair
    * Enable `Protect variable`
* Copy .gitlab-ci.yml in this repo to the source repo and modify `Setup SSH` section and other sections accordingly if you are using other obfuscation library
